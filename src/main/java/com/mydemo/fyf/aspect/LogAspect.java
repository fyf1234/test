package com.mydemo.fyf.aspect;

import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * 日志打印优化
 *
 * @author roamay.com
 * @since 2021/4/28 11:29
 */
@Aspect
@Slf4j
@Component
@Order(5)
public class LogAspect {

    ThreadLocal<Long> startTime = new ThreadLocal<>();

    /**
     * 切点
     */
    @Pointcut("execution(* com.mydemo.fyf.controller..*.*(..))")
    public void forumResponse() {
    }

    /**
     * 切点
     */
    @Pointcut("execution(* com.mydemo.fyf.controller..*.*(..))")
    public void systemResponse() {
    }


    @Before("forumResponse()")
    public void doBeforeOfForumController(JoinPoint joinPoint) {
        doBefore(joinPoint);
    }

    @Before("systemResponse()")
    public void doBeforeOfSystemController(JoinPoint joinPoint) {
        doBefore(joinPoint);
    }

    @AfterReturning(returning = "ret", pointcut = "forumResponse()")
    public void doAfterForumReturning(Object ret) {
        summary(ret);
    }


    @AfterReturning(returning = "ret", pointcut = "systemResponse()")
    public void doAfterSystemReturning(Object ret) {
        summary(ret);
    }

    /**
     * 方法请求后提供方法信息
     *
     * @param ret
     */
    public void summary(Object ret) {
        //处理完请求后，返回内容
        log.info("方法返回值：{}", ret);
        log.info("方法执行时间：{}毫秒", (System.currentTimeMillis() - startTime.get()));
    }


    /**
     * 方法请求打打印日志
     *
     * @param joinPoint
     */
    public void doBefore(JoinPoint joinPoint) {
        //开始计时
        startTime.set(System.currentTimeMillis());
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));//获取请求头中的User-Agent
        //打印请求的内容
        log.info("接口路径：{}", request.getRequestURL().toString());
        log.info("浏览器：{}", userAgent.getBrowser().toString());
        log.info("浏览器版本：{}", userAgent.getBrowserVersion());
        log.info("操作系统: {}", userAgent.getOperatingSystem().toString());
        log.info("IP : {}", request.getRemoteAddr());
        log.info("请求类型：{}", request.getMethod());
        log.info("类方法 : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("请求参数 : {} " + Arrays.toString(joinPoint.getArgs()));
    }
}
