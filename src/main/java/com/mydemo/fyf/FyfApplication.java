package com.mydemo.fyf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import java.util.Arrays;

//改变默认扫描路径(scanBasePackages = "com.mydemo.fyf")
@SpringBootApplication
//开启security注解
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
@MapperScan(basePackages = {"com.mydemo.fyf.mapper"})
public class FyfApplication {

    public static void main(String[] args) {
        SpringApplication.run(FyfApplication.class, args);
    }
}
