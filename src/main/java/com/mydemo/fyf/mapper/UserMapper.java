package com.mydemo.fyf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mydemo.fyf.entity.PermissionEntity;
import com.mydemo.fyf.entity.RoleEntity;
import com.mydemo.fyf.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author fuyufan
 * @date 2021/4/13 15:19
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

    UserEntity getUserByName(String userName);

    List<PermissionEntity> getPermissionByName(Long id);

    List<RoleEntity> getRoleByName(Long id);
}
