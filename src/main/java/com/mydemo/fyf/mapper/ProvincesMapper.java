package com.mydemo.fyf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mydemo.fyf.entity.AppUserCope;
import com.mydemo.fyf.entity.Provinces;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author fuyufan
 * @date 2021/4/13 15:19
 */
@Mapper
public interface ProvincesMapper extends BaseMapper<Provinces> {

}
