package com.mydemo.fyf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mydemo.fyf.entity.AppUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author fuyufan
 * @date 2021/4/13 15:19
 */
@Mapper
public interface AppUserMapper extends BaseMapper<AppUser> {

    void updateDeletedByUserName(List<String> list);
}
