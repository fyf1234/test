package com.mydemo.fyf;

import ch.qos.logback.core.db.DBHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

//改变默认扫描路径(scanBasePackages = "com.mydemo.fyf")
@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {
        //返回IOC容器
        ConfigurableApplicationContext run = SpringApplication.run(TestApplication.class, args);
        //获取容器里的组件
        String[] beanDefinitionNames = run.getBeanDefinitionNames();
        Arrays.stream(beanDefinitionNames).forEach(System.out::println);
        run.getBean("getPerson");
        run.getBean("getPet");
        String[] beanDefinitionNames1 = run.getBeanNamesForType(DBHelper.class);
        Arrays.stream(beanDefinitionNames1).forEach(System.out::println);
    }

}
