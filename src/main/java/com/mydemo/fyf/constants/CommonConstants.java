package com.mydemo.fyf.constants;

/**
 * @ClassName CommonConstants
 * @Description TODO
 * @Author SHUISHUI
 * @Date 2020-08-25 13:06
 * @Version 1.0
 **/
public class CommonConstants {

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 天气对象 redis key
     */
    public static final String WEATHER_INFO_KEY = "weather_info:";

    /**
     * code redis key
     */
    public static final String CODE_KEY = "code_info:";

    // 公共site
    public static final String SITE = "yymh";

    // 加密key
    public static final String AES_KEY = "bluetopo12332188";

    /**
     * 微信授权token redis key
     */
    public static final String WX_TOKEN_KEY = "wx_tokens:";

    /**
     * sms uuid redis key
     */
    public static final String SMS_UUID_KEY = "sms_uuid:";

    /**
     * 健康卡code
     */
    public static final String JKK_CODE = "jkk_code:";

    /**
     * 1 状态码
     */
    public static final Integer COMMON_ONE_CODE = 1;

    /**
     * 2 状态码
     */
    public static final Integer COMMON_ZERO_CODE = 0;

    public static final String ON_LINE = "在线";

    public static final String SERVICE_OBJECT = "在线";
    public static final String AUTH_URL = "https://wg.yueyang.gov.cn/ebus/tifapi/sso/connect/thirdparty/getuserinfo";

    public static final String PHONE_API_KEY = "G4MBxOMYueAYj50twV8iXBb1HPqZiBg5";

    public static final String CHANGE_USER_API_KEY = "G4MBxOMYue2Yj50twC8iXBb1HPqZSBg5";

    public static final String WEIXIN_AUTH_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    public static final String APP_PHONE_LOGIN_URL = "https://f250e924-8ad1-4e25-87d1-86af7dceb9a4.bspapp.com/http/getPhone?access_token=%s&openid=%s&timestamp=%s&sign=%s";

    public static final String END_MESSAGE_CONTEN = "您的短信登录验证码为:%s";

    public static final String FORGET_MESSAGE_CONTEN = "您本次更换手机号码的验证码为:%s，有效期为5分钟。请勿将验证码泄露给他人";

    public static final String END_MESSAGE_URL = "http://112.35.1.155:1992/sms/norsubmit";

    public static final String END_MESSAGE_SUCCESS_CODE = "success";
}
