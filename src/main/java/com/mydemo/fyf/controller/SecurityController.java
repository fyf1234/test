package com.mydemo.fyf.controller;


import com.mydemo.fyf.model.ResultModel;
import com.mydemo.fyf.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;

/**
 * @author fuyufan
 * @date 2021/4/9 15:31
 */
@Slf4j
@RestController
@RequestMapping("/test")
public class SecurityController extends BaseController{

    /*
     * 注解
     * 一、JSR-250注解
     *  @DenyAll 和 @PermitAll  代表拒绝和通过。
     *  @RolesAllowed({"USER", "ADMIN"})
     *  代表标注的方法只要具有USER, ADMIN任意一种权限就可以访问。这里可以省略前缀ROLE_，实际的权限可能是ROLE_ADMIN
     *
     * 二、securedEnabled注解
     *  @Secured("ROLE_ADMIN") 只有对应 角色/权限 的用户才可以调用这些方法
     *
     * 三、prePostEnabled注解 支持EL表达式
     * @PreAuthorize --适合进入方法之前验证授权
     * @PostAuthorize --检查授权方法之后才被执行
     * @PostFilter --在方法执行之后执行，而且这里可以调用方法的返回值，然后对返回值进行过滤或处理或修改并返回
     * @PreFilter --在方法执行之前执行，而且这里可以调用方法的参数，然后对参数值进行过滤或处理或修改
     */

    @Autowired
    UserService userService;

/*    @GetMapping("/hello")
    public ResultModel<Object> login(){
        return success(userService.getUserByName("user"));
    }*/

    @PostMapping("/hello")
    public String login(){
        return "HELLO";
    }

    @GetMapping("/hell")
    //@Secured({"ROLE_role","ROLE_rol"})
    //@DenyAll
    //@PermitAll
    //@RolesAllowed({"fan", "ROLE_role"})
    //@PreAuthorize("hasRole('ROLE_USER')")
    //@PreAuthorize("hasAnyAuthority('fan')")
    public ResultModel<Object> login2(){
        return success(userService.getById(1));
    }


    @GetMapping("/welcome")
    public ResultModel<Object> welcome(){
        return success("welcome!");
    }

    @GetMapping("/user")
    public ResultModel<Object> user(){
        return success("user!");
    }

}
