package com.mydemo.fyf.controller;


import cn.hutool.core.util.StrUtil;
import com.mydemo.fyf.enums.ResponseCodeEnum;
import com.mydemo.fyf.model.ResultModel;
import com.mydemo.fyf.service.AppUserService;
import com.mydemo.fyf.service.AppUserService2;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fuyufan
 * @date 2021/6/21 10:01
 */
@RestController
@RequestMapping("/import")
public class ImportController extends BaseController {

    @Autowired
    AppUserService appUserService;

    @Autowired
    AppUserService2 appUserService2;

    private static final int[] kk = {1, 2, 3};

    @RequestMapping(path = "/data", method = RequestMethod.GET)
    public ResultModel<String> login(@RequestParam String path, @RequestParam Integer start, @RequestParam Integer amount) {

        String string = appUserService.importUser(path, start, amount);
        if (StrUtil.isEmpty(string)) {
            return success("从第" + start + "行开始，导入" + amount + "条数据");
        } else {
            return new ResultModel<>(ResponseCodeEnum.INSERT_EXCEPTION,string);
        }
    }

    @RequestMapping(path = "/copy",method = RequestMethod.GET)
    public ResultModel<Object> copy(@RequestParam Integer amount){
        return null;
    }
}
