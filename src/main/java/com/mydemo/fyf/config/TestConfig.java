package com.mydemo.fyf.config;

import ch.qos.logback.core.db.DBHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author fuyufan
 * @date 2021/5/11 10:58
 */
/*告诉SpringBoot这是一个配置类，配置类本身也是一个组件
 *proxyBeanMethods: 是不是代理bean的方法，默认true
 * Full(true) 全模式 getPerson().getPet() == getPet() 为true
 * Lite(false) 轻量级模式 getPerson().getPet() == getPet() 为false
 *
 * @Import
 * 给容器中自动创建出指定类型的组件，实例名默认为全类名
 */
@Import(DBHelper.class)
@Configuration
//@ConditionalOnBean
public class TestConfig {
    //给容器中添加组件，方法名为组件id，返回类型为组件类型，返回的值就是组件在容器中保存的实例，默认单实例
    @Bean
    public Person getPerson() {
        Person person = new Person();
        person.setPet(getPet());
        return person;
    }

    @Bean
    public Pet getPet() {
        return new Pet("Zhang", 80);
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Pet {
    private String name;
    private Integer age;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class Person {
    private String name;
    private Integer age;
    private Pet pet;
}
