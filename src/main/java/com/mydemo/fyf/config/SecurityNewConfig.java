package com.mydemo.fyf.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fuyufan
 * @date 2021/4/9 17:00
 */
/*
 * 自定义实现类设置
 * 第一步：创建配置类，设置使用哪个UserDetailsService实现类
 * 第二步：编写实现类，返回User对象，User对象有用户名密码和操作步骤权限
 */
//@Configuration
//@EnableWebSecurity
public class SecurityNewConfig extends WebSecurityConfigurerAdapter {

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    /**
     * hasAuthority() 指定具体权限
     * hasAnyAuthority() 指定权限中的某一个
     * hasRole() 指定具体角色
     * hasAnyRole 指定角色中的某一个
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        http.authorizeRequests()
                .antMatchers("/test/**").permitAll()
                .antMatchers("/item/**").permitAll()
                .antMatchers("/workorder/config/list").permitAll()
                .antMatchers("/workorder/manage/overtime/list").permitAll()
                .antMatchers("/evaluation/item").permitAll()
                .antMatchers("/imgValidateCode").permitAll()
                .antMatchers("/user/login").permitAll()
                .antMatchers("/checkSMSValidateCode").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/v2/api-docs**").permitAll()
                .anyRequest().authenticated()
                .and().csrf().disable();

        //自定义403页面
        http.exceptionHandling()
                //没有权限，返回json
                .accessDeniedHandler((request,response,ex) -> {
                    response.setContentType("application/json;charset=utf-8");
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    PrintWriter out = response.getWriter();
                    Map<String,Object> map = new HashMap<>();
                    map.put("code",403);
                    map.put("message", "权限不足");
                    out.write(objectMapper.writeValueAsString(map));
                    out.flush();
                    out.close();
                });

//        http
//                .authenticationProvider(authenticationProvider())
//                .httpBasic()
//                //未登录时，进行json格式的提示
//                .authenticationEntryPoint((request,response,authException) -> {
//                    response.setContentType("application/json;charset=utf-8");
//                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//                    PrintWriter out = response.getWriter();
//                    Map<String,Object> map = new HashMap<>();
//                    map.put("code",403);
//                    map.put("message","未登录");
//                    out.write(objectMapper.writeValueAsString(map));
//                    out.flush();
//                    out.close();
//                });
    }


    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
