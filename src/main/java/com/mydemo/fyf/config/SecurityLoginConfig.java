package com.mydemo.fyf.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fuyufan
 * @date 2021/4/9 16:36
 */

/*
 * Security 配置登录账号密码
 */
//@Configuration
public class SecurityLoginConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //加密密码
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");
        System.out.println(encode);
        auth.inMemoryAuthentication().withUser("fan").password(encode).roles("admin");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        http.formLogin()              //自定义自己编写的登录页面
                //.loginPage("")       //登录页面设置
                .loginProcessingUrl("/test/hello")  //登录访问路径
                //.defaultSuccessUrl("/test/welcome").permitAll() //登录成功之后，跳转页面
                .and().authorizeRequests() //定义
                // .antMatchers("/test/hell").permitAll() //设置那些路径不需要认证
                //.antMatchers("/test/welcome").hasAuthority("role") //需要admin权限才能访问
                //.antMatchers("/test/welcome").hasAnyAuthority("role","fan")
                .antMatchers("/test/welcome").access("hasAnyRole('fan')")
                .anyRequest().authenticated();    //所有请求都可以访问
        //.and().csrf().disable();         //关闭csrf防护

        //自定义403页面
        http.exceptionHandling()
                //没有权限，返回json
                .accessDeniedHandler((request,response,ex) -> {
                    response.setContentType("application/json;charset=utf-8");
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    PrintWriter out = response.getWriter();
                    Map<String,Object> map = new HashMap<>();
                    map.put("code",403);
                    map.put("message", "权限不足");
                    out.write(objectMapper.writeValueAsString(map));
                    out.flush();
                    out.close();
                });
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
