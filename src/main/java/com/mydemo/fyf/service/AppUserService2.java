package com.mydemo.fyf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mydemo.fyf.entity.AppUser;

/**
 * @author fuyufan
 * @date 2021/4/13 15:12
 */
public interface AppUserService2 extends IService<AppUser> {

    String importUser(String path, Integer start, Integer amount);

    String copeUser(Integer amount);
}
