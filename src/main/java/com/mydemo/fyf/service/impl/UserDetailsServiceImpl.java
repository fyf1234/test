package com.mydemo.fyf.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mydemo.fyf.entity.PermissionEntity;
import com.mydemo.fyf.entity.RoleEntity;
import com.mydemo.fyf.entity.UserEntity;
import com.mydemo.fyf.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author fuyufan
 * @date 2021/4/9 17:07
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserMapper userMapper;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //查数据库
        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        UserEntity userEntity = userMapper.selectOne(wrapper);
        if (ObjectUtil.isEmpty(userEntity)) {
            logger.error("用户不存在!");
            throw new UsernameNotFoundException("用户不存在");
        }
        //查询权限
        List<PermissionEntity> permissionByName = userMapper.getPermissionByName(userEntity.getId());
        //查询角色
        List<RoleEntity> roleByName = userMapper.getRoleByName(userEntity.getId());
        // 1. commaSeparatedStringToAuthorityList放入角色时需要加前缀ROLE_，而在controller使用时不需要加ROLE_前缀
        // 2. 放入的是权限时，不能加ROLE_前缀，hasAuthority与放入的权限名称对应即可
        //设置权限和角色
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("fan,ROLE_USER");
        //grantedAuthorities.add()
        roleByName.stream().forEach(t->{
            t.getRoleName();
        });
        //设置权限
        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList("ROLE_fan","ROLE_role","ROLE_yu","c");
        //设置用户和密码
        return new User(userEntity.getUserName(), new BCryptPasswordEncoder().encode(userEntity.getPassword()), authorityList);
    }
}
