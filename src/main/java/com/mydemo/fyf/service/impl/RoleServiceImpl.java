package com.mydemo.fyf.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import com.mydemo.fyf.entity.RoleEntity;
import com.mydemo.fyf.mapper.RoleMapper;
import com.mydemo.fyf.service.RoleService;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author fuyufan
 * @date 2021/4/13 15:16
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, RoleEntity> implements RoleService {

    public static void main(String[] args) {
        Multimap<String, Integer> map2 = TreeMultimap.create();
        //Map<String, Object> map2 = new IdentityHashMap<>();
//        Map<String, Object> map2=new TreeMap(new Comparator<Object>(){
//            @Override
//            public int compare(Object a,Object b){
//                return System.identityHashCode(a)-System.identityHashCode(b);
//            }
//        });


        map2.put(new String("fieldNames"),3);//
        map2.put(new String("fieldValues"),4);//
        map2.put(new String("fieldNames"),5);//
        map2.put(new String("fieldValues"),2);//
        //map2.put("李四", 4);

        for (String s : map2.keySet()) {
            System.out.println(s+"===="+map2.get(s));

        }
    }
}
