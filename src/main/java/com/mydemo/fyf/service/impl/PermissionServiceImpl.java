package com.mydemo.fyf.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mydemo.fyf.entity.PermissionEntity;
import com.mydemo.fyf.entity.RoleEntity;
import com.mydemo.fyf.mapper.PermissionMapper;
import com.mydemo.fyf.mapper.RoleMapper;
import com.mydemo.fyf.service.PermissionService;
import com.mydemo.fyf.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * @author fuyufan
 * @date 2021/4/13 15:16
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, PermissionEntity> implements PermissionService {


}
