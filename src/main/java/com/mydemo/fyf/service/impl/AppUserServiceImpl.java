package com.mydemo.fyf.service.impl;

import cn.hutool.core.util.IdcardUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mydemo.fyf.constants.CommonConstants;
import com.mydemo.fyf.entity.AppUser;
import com.mydemo.fyf.entity.Provinces;
import com.mydemo.fyf.entity.UserEntity;
import com.mydemo.fyf.enums.SexEnum;
import com.mydemo.fyf.enums.UserSourceEnum;
import com.mydemo.fyf.mapper.AppUserCopeMapper;
import com.mydemo.fyf.mapper.AppUserMapper;
import com.mydemo.fyf.mapper.ProvincesMapper;
import com.mydemo.fyf.mapper.UserMapper;
import com.mydemo.fyf.service.AppUserService;
import com.mydemo.fyf.service.UserService;
import com.mydemo.fyf.utils.AESUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

/**
 * @author fuyufan
 * @date 2021/4/13 15:16
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements AppUserService {

    @Autowired
    AppUserMapper appUserMapper;

    @Autowired
    AppUserCopeMapper appUserCopeMapper;

    @Autowired
    ProvincesMapper provincesMapper;

    private static final int[] kk = {1, 2, 3};

    /**
     * @param path   读取路径
     * @param start  开始读取行数
     * @param amount 插入数量
     */
    @Override
    public String importUser(String path, Integer start, Integer amount) {

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));//构造一个BufferedReader类来读取文件
            String s = null;
            int i = 0;
            int k = 1;
            String asd = "";
            while ((s = br.readLine()) != null && i < amount) {//使用readLine方法，一次读一行
                String[] temp;
                String delimeter = ",";
                String idCard;// 指定分割字符
                temp = s.split(delimeter);
                // 分割字符串
                if (i == 0) {
                    idCard = temp[0].trim().substring(1);
                } else {
                    idCard = temp[0].trim();
                }
                System.out.println("id:" + idCard);
                String phone = temp[1].trim();
                System.out.println("phone:" + phone);
                String name = temp[2].trim();
                System.out.println("name:" + name);
                QueryWrapper<Provinces> provincesQueryWrapper = new QueryWrapper<>();
                provincesQueryWrapper.like("province", IdcardUtil.getProvinceByIdCard(idCard));
                List<Provinces> provincesList = provincesMapper.selectList(provincesQueryWrapper);
                //过滤条件
                if (k < start || name.startsWith("1") || phone.startsWith("11") || idCard.contains(" ")) {
                    continue;
                } else {
                    AppUser appUser = new AppUser();
                    appUser.setAuthUid("");
                    appUser.setRealName(name);
                    // 加密
                    appUser.setPhone((AESUtils.encrypt(phone, CommonConstants.AES_KEY)));
                    appUser.setUserName(idCard);
                    // 加密
                    appUser.setIdCard(AESUtils.encrypt(idCard, CommonConstants.AES_KEY));
                    appUser.setServiceObject("human");
                    appUser.setLoginType("mp");
                    appUser.setCType("10");
                    appUser.setLevel("1");
                    // 设置年龄性别
                    if (StringUtils.isNotEmpty(appUser.getUserName())) {
                        appUser.setAge(IdcardUtil.getAgeByIdCard(appUser.getUserName()));
                        if (IdcardUtil.getGenderByIdCard(appUser.getUserName()) == 0) {
                            appUser.setSex(SexEnum.FEMALE.getCode());
                        } else if (IdcardUtil.getGenderByIdCard(appUser.getUserName()) == 1) {
                            appUser.setSex(SexEnum.MAN.getCode());
                        } else {
                            appUser.setSex(SexEnum.UNKOWN.getCode());
                        }
                        // 出生日期
                        appUser.setBirthday(DateFormatUtils.format(IdcardUtil.getBirthDate(appUser.getUserName()), "yyyy-MM-dd"));

                        if (provincesList != null && provincesList.size() > 0) {
                            appUser.setProvinceId(provincesList.get(0).getId());
                        }
                    }
                    appUser.setLastLoginTime(new Date());
                    appUser.setVerified(1);
                    appUser.setAntoFill(0);
                    int index = (int) (Math.random() * kk.length);
                    if (kk[index] == 1) {
                        appUser.setUserSource(UserSourceEnum.WX);
                    } else if (kk[index] == 2) {
                        appUser.setUserSource(UserSourceEnum.IOS);
                    } else {
                        appUser.setUserSource(UserSourceEnum.ANDROID);
                    }
                    try {
                        appUserMapper.insert(appUser);
                    } catch (Exception e) {
                        e.printStackTrace();
                        asd = asd + e.getMessage().split("'")[1].trim() + ",";
                    }
                    i++;
                }
                k++;
            }
            log.debug(asd);
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage().split("'")[1].trim();
        }
        return null;
    }

    @Override
    public String copeUser(Integer amount) {
        saveBatch(null);
        return null;
    }
}
