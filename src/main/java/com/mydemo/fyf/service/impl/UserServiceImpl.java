package com.mydemo.fyf.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mydemo.fyf.entity.UserEntity;
import com.mydemo.fyf.mapper.UserMapper;
import com.mydemo.fyf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author fuyufan
 * @date 2021/4/13 15:16
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserEntity getUserByName(String userName) {
        return userMapper.getUserByName(userName);
    }
}
