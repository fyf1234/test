package com.mydemo.fyf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mydemo.fyf.entity.UserEntity;

/**
 * @author fuyufan
 * @date 2021/4/13 15:12
 */
public interface UserService extends IService<UserEntity> {

    UserEntity getUserByName(String userName);
}
