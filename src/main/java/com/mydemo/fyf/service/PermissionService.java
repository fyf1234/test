package com.mydemo.fyf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mydemo.fyf.entity.PermissionEntity;
import com.mydemo.fyf.entity.RoleEntity;

/**
 * @author fuyufan
 * @date 2021/4/13 15:12
 */
public interface PermissionService extends IService<PermissionEntity> {

}
