package com.mydemo.fyf.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author fuyufan
 * @date 2021/6/16 10:09
 */
@AllArgsConstructor
@Getter
public enum RolePermissionEnum {
    ROLE_ADMIN("ADMIN"),
    ROLE_USER("USER"),
    PERMISSION_ROLE("sys:role:info"),
    PERMISSION_MENU("sys:menu:info"),
    PERMISSION_ALL("sys:info:all"),
    PERMISSION_USER("sys:user:info");
    private String rolePermission;
}
