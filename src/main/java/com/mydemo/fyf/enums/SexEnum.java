package com.mydemo.fyf.enums;

/**
 * @author fuyufan
 * @date 2021/6/21 10:19
 */

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SexEnum {
    UNKOWN(0),
    MAN(1),
    FEMALE(2);

    @EnumValue
    private Integer code;

    private SexEnum(Integer code) {
        this.code = code;
    }

    @JsonValue
    public Integer getCode() {
        return this.code;
    }

    @JsonCreator
    public static SexEnum getByCode(Integer code) {
        SexEnum[] var1 = values();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            SexEnum sex = var1[var3];
            if (sex.getCode().equals(code)) {
                return sex;
            }
        }

        return UNKOWN;
    }
}