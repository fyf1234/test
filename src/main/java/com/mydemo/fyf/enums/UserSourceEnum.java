package com.mydemo.fyf.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum UserSourceEnum {
    WX(1),//微信小程序
    IOS(2),//ios
    ANDROID(3),//安卓
    UNKNOW(4),//未知
    APP(5);//未知


    /**
     * 该注解mybatis-plus会将该属性值存储到数据库字段里
     */
    @EnumValue
    private Integer code;

    private UserSourceEnum(Integer code){
        this.code = code;
    }

    /**
     * 该注解是对象序列化成字符串时枚举值变成Integer,跟@JsonCreator作用相反
     * @return
     */
    @JsonValue
    public Integer getCode() {
        return code;
    }

    /**
     * 该注解会将前端提交过来的Integer值转成对应的枚举,跟@JsonValue作用相反
     * @param code
     * @return
     */
    @JsonCreator
    public static UserSourceEnum getByCode(Integer code){
        for(UserSourceEnum classState : UserSourceEnum.values()){
            if(classState.getCode().equals(code)){
                return classState;
            }
        }
        return null;
    }
}
