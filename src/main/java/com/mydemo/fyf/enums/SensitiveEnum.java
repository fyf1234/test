package com.mydemo.fyf.enums;

/**
 * @author fuyufan
 * @date 2021/4/12 14:59
 */
public enum SensitiveEnum {
    /**
     * 中文名
     */
    CHINESE_NAME,
    /**
     * 身份证
     */
    ID_CARD,
    /**
     * 座机
     */
    FIXED_PHONE,
    /**
     * 手机
     */
    MOBILE_PHONE,
    /**
     * 地址
     */
    ADDRESS,
    /**
     * 电子邮件
     */
    EMAIL,
    /**
     * 银行卡号
     */
    BANK_CARD
}
