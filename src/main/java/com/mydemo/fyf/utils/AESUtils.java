package com.mydemo.fyf.utils;

import com.mydemo.fyf.constants.CommonConstants;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;

public final class AESUtils {
    private static final String CHARSET_NAME = "UTF-8";
    private static final String AES_NAME = "AES";
    public static final String ALGORITHM = "AES/CBC/PKCS7Padding";

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * 加密
     *
     * @param content
     * @param key
     * @return
     */
    public static String encrypt(@NotNull String content, @NotNull String key) {
        byte[] result = null;
        try {
            if(StringUtils.isEmpty(key) || StringUtils.isEmpty(content))
                return StringUtils.EMPTY;
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(CHARSET_NAME), AES_NAME);
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(new byte[16]);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, paramSpec);
            result = cipher.doFinal(content.getBytes(CHARSET_NAME));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new String(Hex.encode(result));
    }

    /**
     * 解密
     *
     * @param content
     * @param key
     * @return
     */
    public static String decrypt(@NotNull String content, @NotNull String key) {
        try {
            if(StringUtils.isEmpty(key) || StringUtils.isEmpty(content))
                return StringUtils.EMPTY;
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(CHARSET_NAME), AES_NAME);
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(new byte[16]);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, paramSpec);
            return new String(cipher.doFinal(Hex.decode(content)), CHARSET_NAME);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return StringUtils.EMPTY;
    }

    public static void main(String[] args) {
        String[] strs = new String[]{"430623198909078322","15200293133",
                "430623197401263047","13397614498",
                "430623199505188319","13575059601",
                "430623199903083731","17752830739",
                "430623196610220014","13974083026",
                "430623197511080030","13574029333",
                "43060219831208746X","13973012369",
                "430602198002028616","13975086909"};
        
        try {
            String key = CommonConstants.AES_KEY;
            
            for (int i = 0; i < strs.length; i += 2) {
                String idNo = encrypt(strs[i], key);
                String phone = encrypt(strs[i + 1], key);
                System.out.println(String.format("update app_user set id_card='%s' where id_card='%s';", idNo, strs[i]));
                System.out.println(String.format("update app_user set phone='%s' where phone='%s';", phone, strs[i + 1]));
            }
            
//            String base64Code = encrypt("123456",key);
//            System.out.println(base64Code);
//            String ke = decrypt(base64Code,key);
//            System.out.println(ke);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}