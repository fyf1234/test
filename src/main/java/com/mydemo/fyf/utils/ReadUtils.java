package com.mydemo.fyf.utils;

import cn.hutool.core.util.IdcardUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mydemo.fyf.constants.CommonConstants;
import com.mydemo.fyf.entity.AppUser;
import com.mydemo.fyf.enums.SexEnum;
import com.mydemo.fyf.enums.UserSourceEnum;
import com.mydemo.fyf.service.AppUserService;
import com.mydemo.fyf.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

/**
 * @author fuyufan
 * @date 2021/6/21 10:33
 */
public class ReadUtils {
    @Autowired
    static UserService userService;
    @Autowired
    static AppUserService appUserService;
    private static  final int[] kk = { 1, 2, 3 };
    public static void getAppUser(String url){

        //读取文件的数据
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(url), "UTF-8"));//构造一个BufferedReader类来读取文件
            String s = null;
            int i = 0;
            while((s = br.readLine())!=null&&i<20){//使用readLine方法，一次读一行
                String[] temp;
                String delimeter = ",";
                String idCard;// 指定分割字符
                temp = s.split(delimeter);
                // 分割字符串
                if(i == 0){
                    idCard = temp[0].trim().substring(1);
                }else{
                    idCard = temp[0].trim();
                }
                System.out.println("id:"+idCard);
                String phone = temp[1].trim();
                System.out.println("phone:"+phone);
                String name = temp[2].trim();
                System.out.println("name:"+name);
                i++;
                //每次导入的条数
                if (i > 10){
                    return ;
                }

                if(name.startsWith("1") || phone.startsWith("11")||idCard.contains(" ")){
                    continue;
                }
                AppUser appUser = new AppUser();
                appUser.setAuthUid("");
                appUser.setRealName(name);
                // 加密
                appUser.setPhone((AESUtils.encrypt(phone, CommonConstants.AES_KEY)));
                appUser.setUserName(idCard);
                // 加密
                appUser.setIdCard(AESUtils.encrypt(idCard, CommonConstants.AES_KEY));
                appUser.setServiceObject("human");
                appUser.setLoginType("mp");
                appUser.setCType("10");
                appUser.setLevel("1");
                // 设置年龄性别

                userService.getUserByName("admin");
                if (StringUtils.isNotEmpty(appUser.getUserName())) {
                    appUser.setAge(IdcardUtil.getAgeByIdCard(appUser.getUserName()));
                    if (IdcardUtil.getGenderByIdCard(appUser.getUserName()) == 0) {
                        appUser.setSex(SexEnum.FEMALE.getCode());
                    } else if (IdcardUtil.getGenderByIdCard(appUser.getUserName()) == 1) {
                        appUser.setSex(SexEnum.MAN.getCode());
                    } else {
                        appUser.setSex(SexEnum.UNKOWN.getCode());
                    }
                    // 出生日期
                    appUser.setBirthday(DateFormatUtils.format(IdcardUtil.getBirthDate(appUser.getIdCard()), "yyyy-MM-dd"));
                    QueryWrapper<AppUser> provincesQueryWrapper = new QueryWrapper<>();
//                    provincesQueryWrapper.like("province", IdcardUtil.getProvinceByIdCard(appUser.getIdCard()));
//                    List<AppUser> provincesList = appUserService.list(provincesQueryWrapper);
//                    if (provincesList != null && provincesList.size() > 0) {
//                        appUser.setProvinceId(provincesList.get(0).getId());
//                    }
                }
                appUser.setLastLoginTime(new Date());
                appUser.setVerified(1);
                appUser.setAntoFill(0);

                int index = (int) (Math.random() * kk.length);

                if (kk[index] == 1) {
                    appUser.setUserSource(UserSourceEnum.WX);
                } else if (kk[index] == 2) {
                    appUser.setUserSource(UserSourceEnum.IOS);
                } else  {
                    appUser.setUserSource(UserSourceEnum.ANDROID);
                }
                appUserService.saveOrUpdate(appUser);
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        //数据解析
    }
}
