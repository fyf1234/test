package com.mydemo.fyf.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author roamay.com
 * @since 2021/4/6
 */

@Slf4j
public class DataBaseUtils {

    public static void main(String[] args) {
        // 可以用 @Slf4j 代替
        Logger log = LoggerFactory.getLogger(Object.class);
        // 数据库连接参数
        String URL = "jdbc:mysql://106.55.6.202:3306/csmp_user?useUnicode=true&characterEncoding=UTF8&serverTimezone=GMT%2B8";
        String NAME = "root";
        String PASSWORD = "Tencent@123!";
        // 查询当前连接数据库的表信息,根据表名查询表信息的的sql
        String databaseStrSql = "SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.`TABLES` WHERE TABLE_SCHEMA=(SELECT DATABASE())";
        String tableStrSql = "SELECT * FROM information_schema.`COLUMNS` WHERE TABLE_SCHEMA=(SELECT DATABASE()) AND TABLE_NAME = ? ";
        // 使用hutool的excel工具建表导出（直接使用poi也可以），随便导出到某一文件夹即可，我导出在桌面
        ExcelWriter writer = ExcelUtil.getWriter("C:\\Users\\bikao\\Desktop\\csmp_user字段清单.xlsx");
        List<List<String>> rows = new ArrayList<>();
        List<String> titleRow = CollUtil.newArrayList("序号","部门/单位简称","表英文名",
                "表中文名", "是否业务表","字段英文名", "字段中文名", "数据类型","长度",
                "精度","空/非空","默认值","是否主键", "是否时间戳","是否更新标志","更新频率","枚举/类型说明","参考标准");
        rows.add(titleRow);
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection coon = DriverManager.getConnection(URL, NAME, PASSWORD);
            Statement statement = coon.createStatement();
            PreparedStatement preparedStatement = coon.prepareStatement(tableStrSql);
            ResultSet databaseResult = statement.executeQuery(databaseStrSql);
            int i =1;
            while (databaseResult.next()) {
                preparedStatement.setString(1, databaseResult.getString("TABLE_NAME"));
                ResultSet tableResult = preparedStatement.executeQuery();
                while (tableResult.next()) {
                    List<String> row = new ArrayList<>();
                    //序号
                    row.add(i+"");
                    i++;
                    //部门、单位简称
                    row.add("");
                    row.add(databaseResult.getString("TABLE_NAME"));
                    row.add(databaseResult.getString("TABLE_COMMENT"));
                    //是否业务表
                    row.add(StrUtil.isBlank(databaseResult.getString("TABLE_COMMENT")) ? "否":"是");
                    String columnName = tableResult.getString("COLUMN_NAME");
                    row.add(columnName);
                    row.add(tableResult.getString("COLUMN_COMMENT"));
                    //数据类型
                    String columnType = tableResult.getString("COLUMN_TYPE");
                    row.add(columnType.split("\\(")[0]);
                    //长度
                    row.add(tableResult.getString("CHARACTER_MAXIMUM_LENGTH"));
                    //精度
                    row.add(tableResult.getString("NUMERIC_PRECISION"));
                    //空/非空
                    String nullAble = tableResult.getString("IS_NULLABLE");
                    row.add(nullAble.equals("YES") ? "是":"否");
                    //默认值
                    row.add(tableResult.getString("COLUMN_DEFAULT"));
                    //是否主键
                    String temp = tableResult.getString("COLUMN_KEY").equals("PRI") ? "是" : "否";
                    row.add(temp);
                    //是否时间戳
                    row.add("");
                    //是否更新标志
                    row.add("");
                    //更新频率
                    row.add("");
                    //枚举/类型说明
                    row.add("");
                    //参考标准
                    row.add(columnName.contains("_time")?"GMT%2B8":"");
                    rows.add(row);
                }
            }

        } catch (ClassNotFoundException e) {
            log.error("驱动加载失败", e);
        } catch (SQLException e) {
            log.error("获取句柄失败", e);
        }
        writer.write(rows,true);
        writer.close();
    }
}

