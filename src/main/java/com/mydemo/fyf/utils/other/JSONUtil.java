package com.mydemo.fyf.utils.other;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * json工具
 *
 * @author haijunjiang
 * @date 2018/12/10
 * @Copyright 2018 www.digitalgd.com.cn Inc. All rights reserved.
 */
@Slf4j
public class JSONUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
    }

    private JSONUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * @param data
     * @throws
     * @Title: toString
     * @Description: java对象转json字符串，默认将驼峰模式转换成下划线
     * @return: String
     */
    public static String toString(Object data) throws Exception {
        return data == null ? "" : objectMapper.writeValueAsString(data);
    }

    /**
     * @param data      需要转换的json字符串
     * @param valueType 需要转换的java对象
     * @throws
     * @Title: toObject
     * @Description: 字符串转换成Object对象
     * @return: String
     */
    public static <T> T toObject(String data, Class<T> valueType) throws Exception {
        return StringUtils.isNotBlank(data) ? objectMapper.readValue(data, valueType) : null;
    }

    /**
     * @param data          需要转换的json字符串
     * @param typeReference 需要转换的java对象
     * @throws
     * @Title: toObject
     * @Description: 字符串转换成Object对象
     * @return: String
     */
    public static <T> T toObject(String data, TypeReference<T> typeReference) throws Exception {
        return StringUtils.isNotBlank(data) ? objectMapper.readValue(data, typeReference) : null;
    }

    /**
     * 将Object对象里面的属性和值转化成Map对象
     *
     * @param obj
     * @return
     * @throws IllegalAccessException
     */
    public static Map<String, Object> obj2Map(Object obj) {
        Map<String, Object> map = new HashMap<>();
        // 获取对象对应类中的所有属性域
        Field[] fields = obj.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            String varName = fields[i].getName();
            //varName=varName.toUpperCase();///将key置为大写，默认为对象的属性
            // 获取原来的访问控制权限
            boolean accessFlag = fields[i].isAccessible();
            // 修改访问控制权限
            fields[i].setAccessible(true);
            try {
                // 获取在对象中属性fields[i]对应的对象中的变量
                Object object = fields[i].get(obj);
                if (object != null) {
                    map.put(varName, object);
                } else {
                    map.put(varName, null);
                }
                // 恢复访问控制权限
                fields[i].setAccessible(accessFlag);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                // TODO Auto-generated catch block
                log.error("JSON转换出现异常: " + e, e);
            }
        }
        return map;
    }
}
