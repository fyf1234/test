package com.mydemo.fyf.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuyufan
 * @date 2021/4/12 14:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user")
public class UserEntity {

    @TableId("id")
    private Long id;

    @TableField("username")
    private String userName;

    private String password;

}
