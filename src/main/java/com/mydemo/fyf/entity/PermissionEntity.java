package com.mydemo.fyf.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fuyufan
 * @date 2021/4/12 14:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_menu")
public class PermissionEntity {

    @TableId("id")
    private Long id;

    private String name;

    private String perms;
}
