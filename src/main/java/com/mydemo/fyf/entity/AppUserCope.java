package com.mydemo.fyf.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mydemo.fyf.enums.UserSourceEnum;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;

import java.util.Date;


@Data
@TableName("app_user_backup2")
public class AppUserCope {
   
    private static final long serialVersionUID = 1L;
   


    @TableLogic
    private Integer deleted;

    @Length(max=50,message="统一认证uid超出长度")
    private String authUid;

    @Length(max=50,message="用户姓名超出长度")
    private String userName;

    @Length(max=50,message="用户密码超出长度")
    private String passWord;

    @Length(max=20,message="真实姓名超出长度")
    private String realName;

    private Integer sex;

    private Integer age;

    @Length(max=20,message="民族超出长度")
    private String nation;

    private String birthday;

    @Length(max=50,message="身份证超出长度")
    private String idCard;

		
    @Length(max=255,message="头像路径超出长度")
    private String avatarUrl;

		
    @Length(max=50,message="手机号码超出长度")
    private String phone;

		
    @Length(max=64,message="电子邮箱超出长度")
    private String email;

    private Integer verified;

    private Integer antoFill;

    @Length(max=50,message="微信openId超出长度")
    private String openId;

    private String loginType;

    private String cType;

    private String level;

		
    @Length(max=50,message="微信号超出长度")
    private String wechat;

		
    private UserSourceEnum userSource;

		
    private Date lastLoginTime;

		
    private String serviceObject;

    private Long provinceId;

    private Long cityId;

    private Long areaId;

    private Integer daysFree;

    private String corpType;

    private String linkPersonCtype;

    private String linkPersonPhone;

    private String linkPersonName;

    private String linkPersonCid;

    private String legalPersonCtype;

    private String legalPersonName;

    private String legalPersonCid;

    @TableField(exist = false)
    private String phoneShow;

    @TableField(exist = false)
    private String cardNoShow;

    private String unionId;

    private String code;

    @TableId(
            value = "id",
            type = IdType.AUTO
    )
    @Id
    private Long id;
    @TableField(
            fill = FieldFill.INSERT
    )
    @JsonIgnore
    private Long createBy;
    @TableField(
            fill = FieldFill.INSERT
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date createTime;
    @TableField(
            fill = FieldFill.UPDATE
    )
    @JsonIgnore
    private Long updateBy;
    @TableField(
            fill = FieldFill.UPDATE
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date updateTime;
}