package com.mydemo.fyf.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@TableName("provinces")
public class Provinces  {
   
    private static final long serialVersionUID = 1L;
   
		
    private Integer provinceid;

		
    @Length(max=100,message="超出长度")
    @NotBlank(message="不能为空")
    private String province;

    @TableId(
            value = "id",
            type = IdType.AUTO
    )
    @Id
    private Long id;

    @TableField(
            fill = FieldFill.INSERT
    )
    @JsonIgnore
    private Long createBy;

    @TableField(
            fill = FieldFill.INSERT
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date createTime;
    @TableField(
            fill = FieldFill.UPDATE
    )
    @JsonIgnore
    private Long updateBy;

    @TableField(
            fill = FieldFill.UPDATE
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date updateTime;
}